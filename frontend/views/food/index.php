<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\select2\Select2;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;



/* @var $this yii\web\View */
/* @var $searchModel common\models\FoodsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Foods';
$this->params['breadcrumbs'][] = $this->title;
$ingr = Yii::$app->request->queryParams;

?>

<div class="foods-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>
    <?= Select2::widget([
        'name' => 'ingredients',
        'data' => $ingredients,
        'size' => Select2::MEDIUM,
        'options' => ['placeholder' => 'Select a state ...', 'multiple' => true],
        'pluginOptions' => [
            'allowClear' => true,
            'maximumSelectionLength'=> 5,
        ],

        'value' => $ingr['ingredients']
    ]); ?>
    <div id="message" style="display: none;"><div class="alert alert-danger" role="alert">Выберите 2 или  больше ингредиентов</div></div>
    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary','id'=>'submitbutton', 'disabled'=>'disabled']) ?>

    </div>

    <?php ActiveForm::end();?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'name',

        ],
    ]); ?>


</div>

