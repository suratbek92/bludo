<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "foods".
 *
 * @property int $id
 * @property string $name
 * @property int $status
 */
class Foods extends \yii\db\ActiveRecord
{
    public $matches;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'foods';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['status'], 'integer'],
            [['name'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'status' => 'Status',
        ];
    }
    public function getFood_ingredients() {
        return $this->hasMany(FoodIngredients::className(), ['food_id'=>'id']);
    }
}
