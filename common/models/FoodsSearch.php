<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Foods;
use yii\helpers\ArrayHelper;

/**
 * FoodsSearch represents the model behind the search form of `common\models\Foods`.
 */
class FoodsSearch extends Foods
{
    public $ingredients;
    public $matches;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ingredients','matches'], 'safe'],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {


        $query = Foods::find();

        $query->joinWith(['food_ingredients']);
        $query->addSelect("`foods`.*, COUNT(`food_ingredients`.`ingredient_id`) as `matches`");

        // add conditions that should always apply here





        $this->load($params,'');
        // grid filtering conditions
        $query->FilterWhere([
            'food_ingredients.ingredient_id' =>$this->ingredients,
        ]);
        $query->having(['>','matches',1]);
        $query->groupBy(['id' ]);
        $query->orderBy ('matches DESC');
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        if($params['ingredients']){

            if(count($params['ingredients'])<2) {
                $query->where('0=1');
            }
            if(max(ArrayHelper::getColumn($dataProvider->getModels(),'matches'))==count($params['ingredients'])) $query->having(['=','`matches`',count($params['ingredients'])]);
            $dataProvider = new ActiveDataProvider([
                'query' => $query,
            ]);
        }
        else {
            $query->where('0=1');
        }


        if (!$this->validate()) {

            // uncomment the following line if you do not want to return any records when validation fails
            $query->where('0=1');
            return $dataProvider;
        }
        //echo $query->createCommand()->sql;
        return $dataProvider;
    }
}
