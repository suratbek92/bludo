<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "food_ingredients".
 *
 * @property int $id
 * @property int $food_id
 * @property int $ingredient_id
 */
class FoodIngredients extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'food_ingredients';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['food_id', 'ingredient_id'], 'required'],
            [['food_id', 'ingredient_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'food_id' => 'Food ID',
            'ingredient_id' => 'Ingredient ID',
        ];
    }
    public function getFoods() {
        return $this->hasMany(Foods::className(), ['id'=>'food_id']);
    }
}
