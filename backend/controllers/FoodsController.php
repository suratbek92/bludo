<?php

namespace backend\controllers;

use common\models\FoodIngredients;
use common\models\Ingredients;
use Yii;
use common\models\Foods;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
 * FoodsController implements the CRUD actions for Foods model.
 */
class FoodsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Foods models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Foods::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Foods model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id){
        $ingredients=ArrayHelper::map(ArrayHelper::toArray(Ingredients::find()->all()),'id', 'name');


$model=$this->findModel($id);
$value='';
$items=ArrayHelper::getColumn(ArrayHelper::toArray($model->food_ingredients),'ingredient_id');

foreach($items  as $key=>$item){
    if($key==0) $value=$ingredients[$item];
    else $value=$value.', '.$ingredients[$item];

}

        return $this->render('view', [
            'model' => $model,
            'value'=>$value
        ]);
    }

    /**
     * Creates a new Foods model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Foods();
        $foodingredients=new FoodIngredients();
        if($model->load(Yii::$app->request->post())) {

            $valid = $model->validate();
            $selected_ingredients=Yii::$app->request->post('ingredients');

            if ($valid) {

                $transaction = \Yii::$app->db->beginTransaction();
                try {

                    if ($flag = $model->save()) {

                        foreach ($selected_ingredients as $selected_ingredient) {

                            $foodingredients->food_id = $model->id;
                            $foodingredients->ingredient_id = $selected_ingredient;
                            if (!($flag = $foodingredients->save())) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                    }
                    if ($flag) {
                        $transaction->commit();
                        return $this->redirect(['index']);
                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                }
            }
        }

        $ingredients=Ingredients::find()->orderBy(['id' => SORT_ASC])->all();
        $ingredients=ArrayHelper::toArray($ingredients);
        $ingredients=ArrayHelper::map($ingredients, 'id', 'name');


        return $this->render('create', [
            'model' => $model,
            'ingredients'=>$ingredients,
        ]);
    }

    /**
     * Updates an existing Foods model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $ingredients=$model->food_ingredients;
        $foodingredients=new FoodIngredients();
        if($model->load(Yii::$app->request->post())) {

            $valid = $model->validate();
            $selected_ingredients=Yii::$app->request->post('ingredients');
            if(count($selected_ingredients)<2) {
                $valid=false;
                $model->addError('ingredients','Ингредиенты должна быт не менее 2');
            }
            if ($valid) {

                $transaction = \Yii::$app->db->beginTransaction();
                try {

                    if ($flag = $model->save()) {

                        if (!empty($ingredients)) {
                            FoodIngredients::deleteAll(['id' => $ingredients]);
                        }
                        if(!empty($selected_ingredients))
                        foreach ($selected_ingredients as $selected_ingredient) {

                            $foodingredients=new FoodIngredients();
                            $foodingredients->food_id = $model->id;
                            $foodingredients->ingredient_id = $selected_ingredient;
                            if (!($flag = $foodingredients->save(false))) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                    }
                    if ($flag) {
                        $transaction->commit();
                        return $this->redirect(['index']);
                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                }
            }
        }
        $ingredients=Ingredients::find()->orderBy(['id' => SORT_ASC])->all();
        $ingredients=ArrayHelper::toArray($ingredients);
        $ingredients=ArrayHelper::map($ingredients, 'id', 'name');

        return $this->render('update', [
            'model' => $model,
            'ingredients'=>$ingredients,
        ]);
    }

    /**
     * Deletes an existing Foods model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Foods model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Foods the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Foods::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
