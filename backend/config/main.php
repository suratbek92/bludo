<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'modules' => [],
    'components' => [
        'request' => [
            'baseUrl'=>'/admin',
            'csrfParam' => '_csrf-backend',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-backend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'advanced-backend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager'    => [
            'class'               => '\yii\web\UrlManager',
            'showScriptName'      => false,
            'enablePrettyUrl'     => true,
            'enableStrictParsing' => true,
            'rules'               => [
                ''                                                => 'site/index',
                '<module>/<controller>'                           => '<module>/<controller>',
                '<module>/<controller>/<action>'                  => '<module>/<controller>/<action>',
                '<module>/<controller>/<action>/<id:\d+>'         => '<module>/<controller>/<action>',
                '<module>/<controller>/<action>/<id:\d+>/<title>' => '<module>/<controller>/<action>',
                '<module>/<controller>/<id:\d+>/<title>'          => '<module>/<controller>/index',

                '<controller>'                           => '<controller>',
                '<controller>/<action>'                  => '<controller>/<action>',
                '<controller>/<action>/<id:\d+>'         => '<controller>/<action>',
                '<controller>/<action>/<id:\d+>/<title>' => '<controller>/<action>',
                '<controller>/<id:\d+>/<title>'          => '<controller>/index',
                '<controller>/<action>/<params:\S+>'     => '<controller>/<action>',
            ],
        ],
    ],
    'params' => $params,
];
